console.log("linkedin-in started");
let canDo = false;

var linkedInInFlag = false;
chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    if (!linkedInInFlag && request.txt === "linkedin.com/in/") {
        setTimeout(function () {
            console.log("linkedin-in is running");
            console.log(request.txt, request.id);
            const emberViews = document.getElementsByClassName("pv-browsemap-section__member-container pv-browsemap-section__member-container-line ember-view");
            const newPeople = [];
            for (let i = 0; i < emberViews.length; i++) {
                if (!emberViews[i]) continue;
                let card = emberViews[i];
                // console.log(i, card);
                let newCard = parseCard(card);
                if (newCard != 0) newPeople.push(newCard);
            }
            console.log(newPeople);
            fetch("https://script.google.com/macros/s/AKfycbxiHhaHZsuKr8GudthTVzv4WDuWymS2SZG6uyOoIQPT4QGn7c8/exec", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json;charset=utf-8",
                },
                body: JSON.stringify(newPeople),
            });
        }, 4000);
    }

  
});

window.onload = function(){
    console.log("linkedin-in is running");
    // console.log(request.txt, request.id);
    const emberViews = document.getElementsByClassName("pv-browsemap-section__member-container pv-browsemap-section__member-container-line ember-view");
    const newPeople = [];
    for (let i = 0; i < emberViews.length; i++) {
        if (!emberViews[i]) continue;
        let card = emberViews[i];
        // console.log(i, card);
        let newCard = parseCard(card);
        if (newCard != 0) newPeople.push(newCard);
    }
    console.log(newPeople);
    fetch("https://script.google.com/macros/s/AKfycbxiHhaHZsuKr8GudthTVzv4WDuWymS2SZG6uyOoIQPT4QGn7c8/exec", {
        method: "POST",
        headers: {
            "Content-Type": "application/json;charset=utf-8",
        },
        body: JSON.stringify(newPeople),
    });
}

function parseCard(_card) {
    card = {};
    console.log(_card);
    let ids = _card.getElementsByTagName("a");
    if (ids[0]) card.id = ids[0].href.split("/in/")[1].split("/")[0];
    else return 0;
    let names = _card.getElementsByClassName("name");
    if (names[0]) card.name = names[0].innerText;
    else return 0;
    let distances = _card.getElementsByClassName("distance-and-badge");
    if (distances[0]) card.distance = distances[0].innerText;
    let titles = _card.getElementsByClassName("pv-browsemap-section__member-headline t-14 t-black t-normal");
    if (titles[0]) card.title = titles[0].innerText;
    return card;
}
