console.log("linkedin bg started");
var oldId = "";
chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
    chrome.tabs.query({ active: true, lastFocusedWindow: true }, (tabs) => {
        let id = "";
        if (tabs.length < 1) return;
        let url = tabs[0].url;
        console.log(url);
        if (url.indexOf("linkedin.com/in/") >= 0) {
            id = url.split("linkedin.com/in/")[1].split("/")[0];
        }

        console.log("before sending id ", id);
        console.log("before sending oldId", oldId);
        msg = {
            txt: "linkedin.com/in/",
            id: id
        };
        if (id !== oldId) {
            oldId = id;
            console.log("sending message with id ", id);
            chrome.tabs.sendMessage(tabId, msg);
        }
    });
});
