chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    console.log("rossko is running");
    const parseDateRossko = (string) =>{
        let newDate = string;
        if( newDate[0] == "~" ) newDate = newDate.slice(1, newDate.length).trim();
        const DAY = parseInt( newDate )+1;
        let MONTH = "01";
        if( newDate.toLowerCase().indexOf( "январ" ) > -1) MONTH = "01";
        else if( newDate.toLowerCase().indexOf( "фев" ) > -1 ) MONTH = "02";
        else if( newDate.toLowerCase().indexOf( "март" ) > -1 ) MONTH = "03";
        else if( newDate.toLowerCase().indexOf( "апрел" ) > -1 ) MONTH = "04";
        else if( newDate.toLowerCase().indexOf( "ма" ) > -1 ) MONTH = "05";
        else if( newDate.toLowerCase().indexOf( "июн" ) > -1 ) MONTH = "06";
        else if( newDate.toLowerCase().indexOf( "июл" ) > -1 ) MONTH = "07";
        else if( newDate.toLowerCase().indexOf( "авгус" ) > -1 ) MONTH = "08";
        else if( newDate.toLowerCase().indexOf( "сент" ) > -1 ) MONTH = "09";
        else if( newDate.toLowerCase().indexOf( "октя" ) > -1 ) MONTH = "10";
        else if( newDate.toLowerCase().indexOf( "нояб" ) > -1 ) MONTH = "11";
        else if( newDate.toLowerCase().indexOf( "декаб" ) > -1 ) MONTH = "12";
        const formattedDate = DAY + "." + MONTH + ".2020";
        return formattedDate;
    }

    const parseFunc = (sections, data) =>{
        for (let iS = 1; iS < sections.length; iS++) {
            console.log( "first cycle 1" )
            const rowS = sections[iS].children;
            if (!rowS) continue;
            const row = rowS[0];
            const dataRow = {};
    
            const colGroups = row.children;
            console.log( "first cycle 2", colGroups.length )
            for (let iCG = 0; iCG < colGroups.length; iCG++) {
                console.log( "seconsd cycle 1" )
                if (!colGroups[iCG]) continue;
                console.log( "seconsd cycle 2" )
    
                const CG = colGroups[iCG];
                if (iCG == 0) {
                    dataRow.brand = CG.innerText.trim();
                }
                if (iCG == 1) {
                    const aTags = CG.getElementsByTagName("a");
                    if (aTags.length < 1) continue;
                    dataRow.number = aTags[0].innerText.trim();
                }
                if (iCG == 2) {
                    dataRow.descH4 = CG.innerText.trim();
                    dataRow.descP = "";
                    dataRow.descManagerComment = "";
                }
                if (iCG == 3) {
                    const div3Wrap = CG.children[0];
                    const dataChilds = div3Wrap.children;
                    const DateAndStore = dataChilds[0].children;
                    
                    let rawDate = DateAndStore[0].innerText.trim();
                    
                    dataRow.arriv = parseDateRossko(rawDate);
                    
                    if( DateAndStore[1] ){
                        dataRow.store = DateAndStore[1].innerText.trim();
                        
                    }
                    else{
                        dataRow.store = "UNDF"
                    }
                    
                    dataRow.supplier = "ROSSKO";
                    // dataRow.avail = dataChilds[1].getElementsByTagName("span")[0].innerText.trim();
                    let availability = dataChilds[1].innerText.trim();
                    if(availability[0] == ">" || availability[0] == "<") availability = availability.slice( 1, availability.length );
                    console.log("availability", availability)
                    dataRow.avail = parseInt(availability);
                    
    
                    const rawPrice = dataChilds[2].innerText;
                    dataRow.price = parseInt(rawPrice.split(",")[0].replace(/\s+/g, ""));
                }
                console.log( dataRow )
            }
            data.push({ ...dataRow });
        }
    }
    // const generalDiv = document.getElementById("skin-layout");
    const mainDiv = document.getElementById("main-group");
    const parentDiv = mainDiv.parentElement;
    const childrenDivs = parentDiv.children;

    console.log(childrenDivs.length);
    /**
     * первый и второй компонент имеют elements section заполненные одинаково
     */
    const mainSections = childrenDivs[0].getElementsByTagName("section");
    const analogSections = childrenDivs[1].getElementsByTagName("section");
    console.log(mainSections.length, analogSections.length);
    // return;
    
    const data = [];
    parseFunc( mainSections, data );
    parseFunc( analogSections, data );
    
    console.log(data);
    sendResponse(data);
    // const div = "<div>Heelog</div>";
    // div.textContent = "Hellog";
    // console.log( div )
    // tables[0].appendChild(div);
});
