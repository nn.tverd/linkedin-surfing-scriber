chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    const blocks = document.getElementsByClassName("card-block");
    const data = [];
    for (let block of blocks) {
        if (!block.getElementsByClassName) continue;
        const rows = block.getElementsByClassName("offer-row");
        console.log(rows.length);
        for (let row of rows) {
            const dataRow = {};
            if (!row.getElementsByClassName) continue;
            const dt_ = row.getElementsByClassName("description-table");
            const dt = dt_ && dt_[0];
            // console.log(dt.innerText);
            const tbodyDesc_ = dt.getElementsByTagName("tbody");
            const tbodyDesc = tbodyDesc_ && tbodyDesc_[0];

            const td_ = dt.getElementsByTagName("td");
	    //console.log( td_ )
	    const art = td_[0].getElementsByTagName("a");
	    //console.log( art )
		
	    const brandNumber = art[0].innerText;
            const br = td_[0].innerText
                .trim()
                .replace(/\n/gi, " ")
                .split(brandNumber);

            const brandName = br[0];
            //const brandNumber = br[1];

            const desc_ = td_[2].innerText.trim();
            // console.log(desc_);

            const ot_ = row.getElementsByClassName("offers-table");
            const ot = ot_ && ot_[0];

            const tbodyOffer_ = ot.getElementsByTagName("tbody");
            const tbodyOffer = tbodyOffer_ && tbodyOffer_[0];

            const offersRows = tbodyOffer.getElementsByTagName("tr");
            console.log("offersRows", offersRows.length);
            for (let offersRow of offersRows) {
                const cells = offersRow.getElementsByTagName("td");
                console.log(cells[4].innerText);
                dataRow.brand = brandName;
                dataRow.number = brandNumber;
                dataRow.descH4 = desc_;
                dataRow.descP = "";
                dataRow.descManagerComment = "";
                dataRow.store = cells[0].innerText === "" ? "UNDF" : cells[0].innerText;
                dataRow.supplier = "Phaeton DC";
                dataRow.avail = parseInt(cells[1].innerText);
                const arrivText = cells[2].innerText;
                dataRow.arriv = arrivText;
                const arrivTextSplitted = arrivText.split("(");
                if (arrivTextSplitted.length > 1) {
                    const dateToAdd = parseInt(arrivTextSplitted[1].split(")")[0]) + 1;
                    const theDate = new Date();
                    const newDate = new Date(theDate.getTime() + dateToAdd * 24 * 60 * 60 * 1000);
                    dataRow.arriv = `${newDate.getDate() < 10 ? `0${newDate.getDate()}` : newDate.getDate()}.${
                        newDate.getMonth() + 1 < 10 ? `0${newDate.getMonth() + 1}` : newDate.getMonth() + 1
                    }.${newDate.getFullYear()}`;
                }
                dataRow.price = parseInt(cells[4].innerText.replace(" ", ""));
                data.push({ ...dataRow });
            }
        }
    }
    sendResponse(data);
});
