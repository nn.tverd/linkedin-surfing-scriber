chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    console.log("armtek is running");
    const table = document.getElementById("component-search-table-SRCDATA");
    const data = [];
    const tbody = table.getElementsByTagName("tbody");
    const offersRows = tbody[0].getElementsByTagName("tr");
    console.log("We are get offersRows.length = ", offersRows.length);
    for (let offersRow of offersRows) {
        const dataRow = {};
        if (offersRow.getAttribute("class")) continue;
        const cells = offersRow.getElementsByTagName("td");
        dataRow.brand = cells[0].innerText;
        dataRow.number = cells[1].innerText;
        dataRow.descH4 = cells[2].innerText;
        dataRow.descP = "";
        dataRow.descManagerComment = "";
        dataRow.store = cells[4].innerText === "" ? "UNDF" : cells[4].innerText.slice(3, cells[4].innerText.length);
        dataRow.supplier = "Armtek";
        dataRow.avail = parseInt(cells[3].innerText);
        dataRow.arriv = cells[6].innerText;
        const latestDate = cells[6].getElementsByTagName("div");
        if (latestDate.length > 1) {
            dataRow.arriv = latestDate[1].innerText + ".2020";
        } else {
            dataRow.arriv = dataRow.arriv.slice(0, 10).trim() + "20";
        }
        console.log("data.arriv", data.arriv);
        dataRow.price = parseInt(cells[7].innerText.replace(" ", ""));
        data.push({ ...dataRow });
    }
    sendResponse(data);
});
