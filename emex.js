chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    console.log("emex is running");

    const parseFunc = (divs, data) => {
        // for (let iS = 0; iS < divs.length; iS++) {
        //     console.log("first cycle 1");
        //     const expLists = divs[iS].getElementsByClassName("expandable-list");
        for (let iEL in divs) {
            const oneRow = divs[iEL];
            console.log("oneRow", oneRow, typeof oneRow);
            if (typeof oneRow !== "object") continue;
            const dataRow = {};
            const brandDetails = oneRow.getElementsByClassName("col column-make-num");
            console.log("brandDetails.length", brandDetails.length);
            if (brandDetails.length < 1) continue;
            const brandName = brandDetails[0].getElementsByTagName("span")[0].innerText.trim();
            const brandNumber = brandDetails[0].getElementsByClassName("detail-numbers")[0].innerText.trim();

            const nameDetails = oneRow.getElementsByClassName("col column-name");
            console.log("nameDetails.length", nameDetails.length);
            if (nameDetails.length < 1) continue;
            const descr = nameDetails[0].innerText.trim();

            console.log("first cycle 2", brandName, brandNumber, descr);
            const otherDetails = oneRow.getElementsByClassName("rows-wrap float-right");
            console.log("otherDetails.length", otherDetails.length);
            if (otherDetails.length < 1) continue;
            const offersRows = otherDetails[0].getElementsByClassName("row row-offer");
            // continue;
            for (let iOR = 0; iOR < offersRows.length; iOR++) {
                console.log("seconsd cycle 1");
                if (!offersRows[iOR]) continue;
                console.log("seconsd cycle 2");
                const othDet = offersRows[iOR];
                dataRow.brand = brandName;
                dataRow.number = brandNumber;
                dataRow.descH4 = descr;
                dataRow.descP = "";
                dataRow.descManagerComment = "";
                dataRow.store = "МОСКВА";
                dataRow.supplier = "EMEX";
                const quantityRaw = othDet.getElementsByClassName("col column-quantity");
                console.log("quantityRaw.length", quantityRaw.length);
                if (quantityRaw.length < 1) continue;
                dataRow.avail = parseInt(quantityRaw[0].innerText) ? parseInt(quantityRaw[0].innerText) : 1;

                const priceRaw = othDet.getElementsByClassName("col column-price");
                console.log("priceRaw.length", priceRaw.length);
                if (priceRaw.length < 1) continue;
                dataRow.price = parseInt(priceRaw[0].innerText.replace(/\s+/g, ""));

                const arrivRaw = othDet.getElementsByClassName("col nowrap column-delivery-days");
                console.log("arrivRaw.length", arrivRaw.length);
                if (arrivRaw.length < 1) continue;
                arrivRaw2 = parseInt(arrivRaw[0].innerText) + 1;
                console.log("arrivRaw2", arrivRaw2);
                const theDate = new Date();
                const newDate = new Date(theDate.getTime() + arrivRaw2 * 24 * 60 * 60 * 1000);
                dataRow.arriv = `${newDate.getDate() < 10 ? `0${newDate.getDate()}` : newDate.getDate()}.${
                    newDate.getMonth() + 1 < 10 ? `0${newDate.getMonth() + 1}` : newDate.getMonth() + 1
                }.${newDate.getFullYear()}`;
                console.log(dataRow);
                data.push({ ...dataRow });
            }
            //         }
        }
    };
    // const generalDiv = document.getElementById("skin-layout");
    const mainDivs = document.getElementsByClassName("detail-list");
    // const parentDiv = mainDiv.parentElement;
    // const childrenDivs = parentDiv.children;

    console.log(mainDivs.length);

    const data = [];
    for (let iS = 0; iS < mainDivs.length; iS++) {
        console.log("first cycle 1");
        const expLists = mainDivs[iS].getElementsByClassName("expandable-list");
        parseFunc(expLists, data);
        const analogLists = mainDivs[iS].getElementsByClassName("analog-small-group");
        parseFunc(analogLists, data);
    }

    console.log(data);
    sendResponse(data);
    // const div = "<div>Heelog</div>";
    // div.textContent = "Hellog";
    // console.log( div )
    // tables[0].appendChild(div);
});
